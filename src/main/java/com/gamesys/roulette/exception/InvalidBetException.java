package com.gamesys.roulette.exception;

public class InvalidBetException extends Exception {

    public InvalidBetException(String e) {
        super(e);
    }

}
