package com.gamesys.roulette;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

import com.gamesys.roulette.domain.model.Player;
import com.gamesys.roulette.readers.PlayerReader;

/**
 * Main starting class for the roulette game
 * <p>
 * Register the players via {@link PlayerReader} from the given
 * {@link PLAYERS_FILE} file and execute the game.
 * 
 */
public class Starter {

    private static String PLAYERS_FILE = "Players.txt";

    private static PlayerReader playerReader;

    public static void main(String[] args) throws FileNotFoundException {
        File iFile = new File(PLAYERS_FILE);
        if (!iFile.exists())
            throw new IllegalArgumentException("\nFile " + iFile + "\" does not exists");
        playerReader = new PlayerReader(new BufferedReader(new FileReader(iFile)));
        RouletteGame game = new RouletteGame();
        // starting the game with registered players
        game.start(readPlayers());
    }

    public static List<Player> readPlayers() {
        List<Player> players = new ArrayList<Player>();
        for (Player player : playerReader) {
            players.add(player);
        }
        return players;
    }

}
