package com.gamesys.roulette.readers;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.gamesys.roulette.domain.model.Player;

/**
 * Class for processing players from given stream.
 * 
 * <p>
 * Decorate standard {@code BufferedReader} and recognize {@link Player} from
 * {@link BufferedReader#readLine()}.
 * 
 * <p>
 * Additionally provide functionality for constructing {@link Player} objects.
 * 
 * @see Player
 * 
 */
public class PlayerReader extends BufferedReader implements Iterable<Player> {
    
    /**
     * Standard buffered reader
     */
    protected BufferedReader br;
    
    /**
     * Internal players created by iteration. E.g. from file stream.
     */
    private List<Player> internalPlayers;
    
    /**
     * Construct this object by decorating standard buffer reader
     */
    public PlayerReader(BufferedReader in) {
        super(in);
        this.br = in;
    }
    
    public Player readPlayer(String line)  {
        String playerName = line.trim();
        return new Player(playerName);
    }

    @Override
    public Iterator<Player> iterator() {
        internalPlayers = new ArrayList<Player>();
        try {
            iterate();
        } catch (IOException e) {
            return null;
        }
        return internalPlayers.iterator();
    }
    
    /**
     * Keeps players in internal structure for later use
     * 
     * @throws IOException
     */
    private void iterate() throws IOException {
        try {

            String line;

            while ((line = br.readLine()) != null) {
                internalPlayers.add(readPlayer(line));
            }

        } finally {
            br.close();
        }

    }

}
