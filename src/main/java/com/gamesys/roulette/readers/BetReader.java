package com.gamesys.roulette.readers;

import java.io.BufferedReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import com.gamesys.roulette.domain.model.Bet;
import com.gamesys.roulette.domain.model.BetType;
import com.gamesys.roulette.domain.model.Player;
import com.gamesys.roulette.domain.model.PlayerBet;
import com.gamesys.roulette.exception.InvalidBetException;

public class BetReader extends BufferedReader {

    /**
     * Standard buffered reader
     */
    protected BufferedReader br;

    List<Player> players;

    /**
     * Current player bet string being processed
     */
    public String currentPlayerBet;

    private static final String SPACE = " ";

    public BetReader(BufferedReader in, List<Player> players) {
        super(in);
        this.br = in;
        this.players = new ArrayList<Player>(players);
    }

    /**
     * Read player bet from line provided by {@link BufferedReader} class.
     * 
     * Bet structure:
     * [Player] [BetType(Number, Even, ODD)] [Amount]
     * 
     * @see BufferedReader#readLine()
     * @see
     * 
     * @return
     */
    public PlayerBet readBet() throws InvalidBetException {
        try {
            while ((isEmpty(currentPlayerBet = br.readLine())))
                ;
            if (currentPlayerBet.contains("\n"))
                return null;

            StringTokenizer st = new StringTokenizer(currentPlayerBet, SPACE);

            // parse player
            Player player;
            Bet bet;
            String playerName = st.nextToken();
            player = new Player(playerName);
            if (!players.contains(player)) {
                throw new InvalidBetException("Player " + playerName + " not registered");
            }

            // parse bet type
            String betToken = st.nextToken();
            int betNumber = 0;
            BetType betType;
            if (isNumber(betToken)) {
                betNumber = Integer.valueOf(betToken);
                if (betNumber < 1 || betNumber > 37) {
                    throw new InvalidBetException("Bet Number must be > 0 and < 38");
                }
                betType = BetType.NONE;

            } else {
                betType = BetType.valueOf(betToken);
            }

            // parse amount to bet
            BigDecimal amount = new BigDecimal(st.nextToken());

            // construct bet
            if (betType.equals(BetType.NONE)) {
                bet = Bet.withBetNumber(betNumber, amount);
            } else if (betType.equals(BetType.EVEN) || betType.equals(BetType.ODD)) {
                bet = Bet.withOddEvenBet(betType, amount);
            } else {
                throw new InvalidBetException("You can only bet on ODD | EVEN or Number");
            }

            return new PlayerBet(player, bet);

        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    public boolean isEmpty(final CharSequence cs) {
        return cs == null || cs.length() == 0;
    }

    /**
     * Check whether the given token is a number
     * 
     * @param token
     * @return
     */
    private boolean isNumber(String token) {
        try {
            Double.valueOf(token);
            return true;
        } catch (NumberFormatException e) {

        }
        return false;
    }

}
