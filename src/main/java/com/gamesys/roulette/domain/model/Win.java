package com.gamesys.roulette.domain.model;

import java.math.BigDecimal;

public class Win {

    private final PlayerBet bet;
    private final BigDecimal winningAmount;

    public Win(PlayerBet bet, BigDecimal winningAmount) {
        this.bet = bet;
        this.winningAmount = winningAmount;
    }

    public PlayerBet getBet() {
        return bet;
    }

    public BigDecimal getWinningAmount() {
        return winningAmount;
    }

}
