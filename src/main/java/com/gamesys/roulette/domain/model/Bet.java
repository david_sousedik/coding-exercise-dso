package com.gamesys.roulette.domain.model;

import java.math.BigDecimal;

public class Bet {

    private final BetType type;
    private final int number;
    private final BigDecimal amount;

    public Bet(BetType type, int number, BigDecimal amount) {
        this.type = type;
        this.number = number;
        this.amount = amount;
    }

    public static Bet withBetNumber(int betNumber, BigDecimal betAmount) {
        return new Bet(BetType.NONE, betNumber, betAmount);
    }

    public static Bet withOddEvenBet(BetType betType, BigDecimal betAmount) {
        return new Bet(betType, 0, betAmount);
    }

    public boolean isOddOrEvenBet() {
        return type != BetType.NONE;
    }

    public BetType getType() {
        return type;
    }

    public int getNumber() {
        return number;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    @Override
    public String toString() {
        return "Bet " + (isOddOrEvenBet() ? type : number) + " with amount " + amount;
    }

}
