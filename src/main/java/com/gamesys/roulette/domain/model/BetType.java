package com.gamesys.roulette.domain.model;

public enum BetType {
    
    EVEN, ODD, NONE

}
