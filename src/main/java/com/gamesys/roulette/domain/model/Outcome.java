package com.gamesys.roulette.domain.model;

public enum Outcome {
    
    LOSE, WIN

}
