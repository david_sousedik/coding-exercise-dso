package com.gamesys.roulette.domain.model;

public final class PlayerStandings {

    private final Player player;
    private final BetSummary betSummary;
    private final WinSummary winSummary;

    public PlayerStandings(Player player, BetSummary betSummary, WinSummary winSummary) {
        this.player = player;
        this.betSummary = betSummary;
        this.winSummary = winSummary;
    }

    public Player getPlayer() {
        return player;
    }

    public BetSummary getBetSummary() {
        return betSummary;
    }

    public WinSummary getWinSummary() {
        return winSummary;
    }

}
