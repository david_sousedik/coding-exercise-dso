package com.gamesys.roulette.utils;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.gamesys.roulette.domain.model.Bet;
import com.gamesys.roulette.domain.model.Outcome;
import com.gamesys.roulette.domain.model.Player;
import com.gamesys.roulette.domain.model.PlayerBet;
import com.gamesys.roulette.domain.model.Win;

public class ResultBoard {

    private List<Player> players;

    public ResultBoard(List<Player> players) {
        this.players = players;
    }
    
    public void announceWinningPocket(int result, List<Win> wins, List<PlayerBet> bets) {
        System.out.printf("Number:  %d\n ", result);
        System.out.printf("%-8s %-4s %-4s %-4s\n", "Player", "Bet", "Outcome", "Winnings");
        System.out.printf("---\n");

        Map<String, Win> winsByPlayer = new HashMap<String, Win>(wins.size());
        for (Win win : wins) {
            Player player = win.getBet().getPlayer();
            winsByPlayer.put(player.getName(), win);
        }

        Map<String, PlayerBet> betsByPlayer = new HashMap<String, PlayerBet>(wins.size());
        for (PlayerBet bet : bets) {
            Player player = bet.getPlayer();
            betsByPlayer.put(player.getName(), bet);
        }

        for (Player player : players) {
            String name = player.getName();

            PlayerBet playerBet = betsByPlayer.get(name);
            Bet bet = playerBet != null ? playerBet.getBet() : null;
            if(bet == null) continue;

            Win win = winsByPlayer.get(name);
            Outcome outcome = win != null ? Outcome.WIN : Outcome.LOSE;

            String winAmount = win != null ? win.getWinningAmount().toPlainString() : Float.valueOf(BigDecimal.ZERO.floatValue()).toString();

            System.out.printf("%-10s %-4s %-4s %-8s\n", name, bet.isOddOrEvenBet() ? bet.getType() : bet.getNumber() , outcome, winAmount);
        }
    }

}
