package com.gamesys.roulette.utils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.gamesys.roulette.domain.model.BetType;
import com.gamesys.roulette.domain.model.PlayerBet;
import com.gamesys.roulette.domain.model.Win;

/**
 * Responsible for winning strategy 
 *
 */
public class BetsEvaluator {

    public static final int BET_AMOUNT_MULTIPLE = 36;
    public static final int EVEN_ODD_MULTIPLE = 2;

    /**
     * Evaluate bets for the defined algorithm
     * 
     * @param bets the players bets {@link PlayerBet}
     * @param number the winning number
     * @return list of winnings {@link Win}
     */
    public List<Win> evaluateBets(List<PlayerBet> bets, int number) {

        boolean isEven = number % 2 == 0;

        List<Win> winnings = new ArrayList<Win>();

        for (PlayerBet bet : bets) {

            BigDecimal winAmount = null;

            if (bet.getBet().isOddOrEvenBet()) {
                if (isEven && bet.getBet().getType().equals(BetType.EVEN)
                        || !isEven && bet.getBet().getType().equals(BetType.ODD)) {
                    winAmount = bet.getBet().getAmount().multiply(BigDecimal.valueOf(EVEN_ODD_MULTIPLE));
                }
            } else {
                if (bet.getBet().getNumber() == number) {
                    winAmount = bet.getBet().getAmount().multiply(BigDecimal.valueOf(BET_AMOUNT_MULTIPLE));
                }
            }

            if (winAmount != null) {
                Win win = new Win(bet, winAmount);
                winnings.add(win);
            }
        }

        return winnings;
    }

}
