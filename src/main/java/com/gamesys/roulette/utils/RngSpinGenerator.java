package com.gamesys.roulette.utils;

import java.util.Random;

public class RngSpinGenerator {
    
    private static int MIN = 1;

    private static int MAX = 36;
    
    public int spin() {

        if (MIN >= MAX) {
            throw new IllegalArgumentException("max must be greater than min");
        }

        Random r = new Random();
        return r.nextInt((MAX - MIN) + 1) + MIN;
    }

}
