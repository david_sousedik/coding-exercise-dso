package com.gamesys.roulette;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import com.gamesys.roulette.domain.model.Player;
import com.gamesys.roulette.domain.model.PlayerBet;
import com.gamesys.roulette.domain.model.Win;
import com.gamesys.roulette.exception.InvalidBetException;
import com.gamesys.roulette.readers.BetReader;
import com.gamesys.roulette.utils.BetsEvaluator;
import com.gamesys.roulette.utils.ResultBoard;
import com.gamesys.roulette.utils.RngSpinGenerator;

/**
 * Represents one roulette game with multiple spins.
 * 
 * <p>
 * This class is basically single thread executor which periodically collects
 * bets, finds winning number and calculate winners.
 * 
 * <p>
 * Game choose every {@link ROUND_LENGTH} a random number from the ball. Then
 * {@link BetReader} is responsible for taking the player's bets. Bet structure:
 * [Player] [BetType(Number, Even, ODD)] [Amount]
 * 
 * @see RngSpinGenerator
 * @see BetsEvaluator
 * @see ResultBoard
 */
public class RouletteGame extends TimerTask {

    private static final int ROUND_LENGTH = 30;
    private static final int INITIAL_ROUND_LENGTH = 30;

    private BetReader betReader;

    private final Lock lock = new ReentrantLock();
    private final List<PlayerBet> currentBets = new ArrayList<PlayerBet>();

    private RngSpinGenerator spinGenerator;
    private BetsEvaluator betsEvaluator;
    private ResultBoard resultBoard;

    /**
     * Default constructor
     */
    public RouletteGame() {
        betsEvaluator = new BetsEvaluator();
        spinGenerator = new RngSpinGenerator();
    }

    /**
     * Starting the roulette game, schedule betting rounds and start taking the
     * bets.
     */
    public void start(List<Player> players) {
        this.betReader = new BetReader(new BufferedReader(new InputStreamReader(System.in)), players);
        resultBoard = new ResultBoard(players);
        System.out.println("Starting game with " + players.size() + " registered players");
        scheduleBettingRounds();
        startTakingBets();
    }

    /**
     * Schedule wheel spin iteratively
     */
    private void scheduleBettingRounds() {
        Timer timer = new Timer(true);
        timer.scheduleAtFixedRate(this, INITIAL_ROUND_LENGTH * 1000, ROUND_LENGTH * 1000);
    }

    /** Announce to start puting bets */
    private void startTakingBets() {
        System.out.println("Please enter your bet in the format: NAME NUMBER|ODD|EVEN AMT:\n");
        while (true) {
            try {
                placeBet();
            } catch (InvalidBetException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Periodically choose a random number, evaluate bets and announce winners.
     */
    @Override
    public void run() {
        System.out.println("******** Betting Round Over *********");
        try {
            lock.lock();
            int number = spinGenerator.spin();
            List<PlayerBet> bets = collectBets();
            List<Win> wins = betsEvaluator.evaluateBets(bets, number);
            resultBoard.announceWinningPocket(number, wins, bets);
        } finally {
            lock.unlock();
        }
    }

    /** Collecting bets and create shallow copy */
    private List<PlayerBet> collectBets() {
        ArrayList<PlayerBet> betsCopy = new ArrayList<PlayerBet>(currentBets);
        currentBets.clear();
        return betsCopy;
    }

    /**
     * Placing the bet from player
     * 
     * @throws InvalidBetException
     */
    private void placeBet() throws InvalidBetException {
        PlayerBet bet = betReader.readBet();
        System.out.println(bet.toString());
        System.out.println();
        try {
            lock.lock();
            currentBets.add(bet);
        } finally {
            lock.unlock();
        }
    }

}
