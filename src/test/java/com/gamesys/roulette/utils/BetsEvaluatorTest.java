package com.gamesys.roulette.utils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.gamesys.roulette.domain.model.Bet;
import com.gamesys.roulette.domain.model.BetType;
import com.gamesys.roulette.domain.model.Player;
import com.gamesys.roulette.domain.model.PlayerBet;
import com.gamesys.roulette.domain.model.Win;
import com.gamesys.roulette.utils.BetsEvaluator;

public class BetsEvaluatorTest {

    BetsEvaluator betsEvaluator = new BetsEvaluator();
    List<PlayerBet> bets = new ArrayList<PlayerBet>();

    @Before
    public void setUp() {
        Player player1 = new Player("Tiki_Monkey");
        Bet bet1 = Bet.withBetNumber(2, new BigDecimal("1.0"));
        PlayerBet playerBet1 = new PlayerBet(player1, bet1);

        Player player2 = new Player("Barbara");
        Bet bet2 = Bet.withOddEvenBet(BetType.EVEN, new BigDecimal("3.0"));
        PlayerBet playerBet2 = new PlayerBet(player2, bet2);

        bets.add(playerBet1);
        bets.add(playerBet2);
    }

    @Test
    public void whenWrongBetsThenNoWinnings() {
        int winningNumber = 1;
        List<Win> wins = betsEvaluator.evaluateBets(bets, winningNumber);
        Assert.assertEquals("No winnings expected", 0, wins.size());
    }

    @Test
    public void whenEvenBetThenOneWinning() {
        int winningNumber = 6;
        List<Win> wins = betsEvaluator.evaluateBets(bets, winningNumber);
        Assert.assertEquals("One winning expected", 1, wins.size());
    }

    @Test
    public void whenCorrectNumberAndEvenBetThenTwoWinnings() {
        int winningNumber = 2;
        List<Win> wins = betsEvaluator.evaluateBets(bets, winningNumber);
        Assert.assertEquals("Two winnings expected", 2, wins.size());
    }

}
