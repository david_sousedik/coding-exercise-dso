package com.gamesys.utils.readers;

import java.io.BufferedReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.gamesys.roulette.domain.model.Bet;
import com.gamesys.roulette.domain.model.Player;
import com.gamesys.roulette.domain.model.PlayerBet;
import com.gamesys.roulette.exception.InvalidBetException;
import com.gamesys.roulette.readers.BetReader;

public class BetReaderTest {

    BetReader reader = null;
    List<Player> players = null;
    BufferedReader bufferedReader = null;
    Player player1 = new Player("Tiki_Monkey");
    Player player2 = new Player("Barbara");
    Bet bet = Bet.withBetNumber(2, new BigDecimal("1.0"));

    @Before
    public void setUp() {
        players = Arrays.asList(player1, player2);
        bufferedReader = org.mockito.Mockito.mock(BufferedReader.class);
    }

    @Test(expected = InvalidBetException.class)
    public void whenInvalidPlayerThenInvalidBetException() throws IOException, InvalidBetException {
        try {
            org.mockito.Mockito.when(bufferedReader.readLine()).thenReturn("Invalid_player 3 1.0");
            reader = new BetReader(bufferedReader, players);
            PlayerBet bet = reader.readBet();
        } finally {
            reader.close();
        }
    }

    @Test
    public void whenValidBetThenPlayerBet() throws IOException, InvalidBetException {
        org.mockito.Mockito.when(bufferedReader.readLine()).thenReturn("Tiki_Monkey 2 1.0");
        reader = new BetReader(bufferedReader, players);
        PlayerBet playerBet = reader.readBet();
        Assert.assertEquals(player1, playerBet.getPlayer());
        Assert.assertEquals(bet.getNumber(), playerBet.getBet().getNumber());
        Assert.assertEquals(bet.getAmount(), playerBet.getBet().getAmount());
    }

}
